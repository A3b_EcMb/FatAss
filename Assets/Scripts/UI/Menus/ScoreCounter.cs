﻿using TMPro;
using UnityEngine;

namespace Scripts
{
    public class ScoreCounter : MonoBehaviour
    {
        public int PassedSeconds { get; private set; }
        
        public TextMeshProUGUI text;

        private float _passedTime;
        private bool _paused;

        private void Start()
        {
            Utils.LevelPaused += value => _paused = value;
            Utils.LevelRestarted += () => OnEnable();
            Utils.PlayerDied += () => _paused = true;
        }

        private void OnEnable()
        {
            _paused = false;
            _passedTime = 0f;
            PassedSeconds = 0;
            text.text = "0";
        }

        private void Update()
        {
            if (_paused) return;
            
            _passedTime += Time.deltaTime;
            int seconds = Mathf.RoundToInt(_passedTime);
            if (PassedSeconds == seconds) return;

            PassedSeconds = seconds;
            text.text = PassedSeconds.ToString();
        }
    }
}