﻿using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class PauseMenu : MonoBehaviour
    {
        public Button[] closeButtons;
        public Button homeButton;
        public Button replayButton;
        public UiAnimator animator;

        private void Start()
        {
            foreach (var button in closeButtons)
            {
                button.onClick.AddListener(() =>
                {
                    SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickButton);
                    Utils.LevelPaused?.Invoke(false);
                    animator.Disable();
                });
            }
            
            homeButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.BackToMenu);
                animator.Disable();
                gameObject.SetActive(false);
                Utils.LevelPaused?.Invoke(false);
                Utils.LevelExit?.Invoke();
            });
            replayButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.StartPlay);
                animator.Disable();
                Utils.LevelRestarted?.Invoke();
                Utils.LevelPaused?.Invoke(false);
            });
        }

        public void Enable()
        {
            animator.Enable();
        }
    }
}