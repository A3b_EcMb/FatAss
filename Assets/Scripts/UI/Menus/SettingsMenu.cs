﻿using System;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace Scripts
{
    public class SettingsMenu : MonoBehaviour
    {
        public Action Closed;
        
        public Slider volumeSlider;
        public Button soundButton;
        public GameObject soundOffIcon;
        public AudioMixer mixer;
        public UiAnimator animator;

        public Button[] closeButtons;

        public float minVolume;
        public float maxVolume;

        private bool _muted;
        private float _volume;
        
        private bool _readedVolume;

        private float Volume => Mathf.Lerp(minVolume, maxVolume, _volume);

        public bool Muted
        {
            get => _muted;
            private set
            {
                _muted = value;
                soundOffIcon.SetActive(value);
            }
        }

        private void Awake()
        {
            volumeSlider.onValueChanged.AddListener(value =>
            {
                _volume = 1 - Mathf.Pow(1 - value, 4);
                Muted = value == 0f;
                
                mixer.SetFloat("Master Volume", Volume);
            });
            
            foreach (var button in closeButtons)
            {
                button.onClick.AddListener(() =>
                {
                    SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickButton);
                    animator.Disable();
                    Closed?.Invoke();
                });
            }
            
            soundButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickButton);
                ToggleSounds();
            });
            Muted = false;
            
            ReadVolume();
            volumeSlider.value = 1 - Mathf.Pow(1 - _volume, 4);
        }

        private void ReadVolume()
        {
            mixer.GetFloat("Master Volume", out float volume);
            _volume = Mathf.InverseLerp(minVolume, maxVolume, volume);
        }
        
        public void ToggleSounds()
        {
            Muted = !Muted;
            
            if (!_readedVolume)
            {
                ReadVolume();
                _readedVolume = true;
            }
            
            mixer.SetFloat("Master Volume", Muted ? -80f : Volume);
        }
    }
}