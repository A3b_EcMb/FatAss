﻿using UnityEngine;

namespace Scripts
{
    public class GameUiStarter : MonoBehaviour
    {
        public GameObject[] itemsToEnable;
        public GameObject[] itemsToDisable;

        private void Awake()
        {
            foreach (var item in itemsToEnable)
            {
                item.SetActive(true);
            }

            foreach (var item in itemsToDisable)
            {
                item.SetActive(false);
            }
        }
    }
}