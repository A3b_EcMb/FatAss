﻿using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class GameOverMenu : MonoBehaviour
    {
        private static int _failes;
        
        public Button homeButton;
        public Button replayButton;
        public ScoreCounter scoreCounter;
        public TextMeshProUGUI scoreText;
        public UiAnimator animator;
        public GameObject newTopScoreText;
        public GameObject[] scoreTextObjects;

        private void Start()
        {
            homeButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.BackToMenu);
                animator.Disable();
                gameObject.SetActive(false);
                Utils.LevelPaused?.Invoke(false);
                Utils.LevelExit?.Invoke();
            });
            replayButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.StartPlay);
                animator.Disable();
                Utils.LevelRestarted?.Invoke();
                Utils.LevelPaused?.Invoke(false);
            });
        }

        public void Enable()
        {
            newTopScoreText.SetActive(false);
            foreach (var scoreTextObject in scoreTextObjects)
            {
                scoreTextObject.SetActive(true);
            }
            
            animator.Enable();
            int score = scoreCounter.PassedSeconds;
            scoreText.text = score.ToString();

            int topScore = Utils.GetTopScore();
            if (score > topScore)
            {
                Utils.SetTopScore(score);
                newTopScoreText.SetActive(true);
                foreach (var scoreTextObject in scoreTextObjects)
                {
                    scoreTextObject.SetActive(false);
                }
            }

            _failes++;
            if (_failes < Utils.FailesToShowAd) return;

            _failes = 0;
            AdsPlayer.Instance.ShowInterstitial();
        }
    }
}