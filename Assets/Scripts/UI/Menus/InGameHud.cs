﻿using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class InGameHud : MonoBehaviour
    {
        public Button pauseButton;
        public Button soundButton;
        public GameObject soundsOffImage;
        
        public PauseMenu pauseMenu;
        public GameOverMenu gameOverMenu;
        public SettingsMenu settingsMenu;

        private void Start()
        {
            pauseButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickButton);
                pauseMenu.Enable();
                Utils.LevelPaused?.Invoke(true);
            });
            soundButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickButton);
                settingsMenu.ToggleSounds();
                soundsOffImage.SetActive(settingsMenu.Muted);
            });

            Utils.PlayerDied += () => gameOverMenu.Enable();
            
            soundsOffImage.SetActive(settingsMenu.Muted);
        }
    }
}