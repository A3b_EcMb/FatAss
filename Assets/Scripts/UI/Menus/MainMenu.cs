﻿using Data;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class MainMenu : MonoBehaviour
    {
        public UiAnimator selfAnimator;
        
        [Header("Play")]
        public Button playButton;
        public UiAnimator gameUi;
        public Level level;
        
        [Header("Settings")]
        public Button settingsButton;
        public UiAnimator settingsMenuAnimator;
        public SettingsMenu settingsMenu;

        [Header("Tutorial")]
        public Button tutorialButton;
        public UiAnimator tutorialMenuAnimator;
        public TutorialMenu tutorialMenu;

        [Header("Character fat")]
        public float defaultFat;
        public float fatChangeValue;
        public Image characterImage;
        public CharacterData characterData;
        public Image fatSlider;
        public Button addFatButton;
        public Button reduceFatButton;

        private float _characterFat;
        
        private void Start()
        {
            _characterFat = defaultFat;
            UpdateFat();
            
            addFatButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickButton);
                _characterFat += fatChangeValue;
                UpdateFat();
            });
            reduceFatButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickButton);
                _characterFat -= fatChangeValue;
                UpdateFat();
            });
            
            playButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.StartPlay);
                selfAnimator.Disable();
                
                if (Utils.PlayerViewedTutorial())
                {
                    gameUi.Enable();
                    Utils.LevelRestarted?.Invoke();
                }
                else
                {
                    Utils.SetPlayerViewedTutorial();
                    tutorialMenuAnimator.Enable();
                    tutorialMenu.Closed = () =>
                    {
                        gameUi.Enable();
                        Utils.LevelRestarted?.Invoke();
                    };
                }
            });
            
            settingsButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickButton);
                selfAnimator.Disable();
                settingsMenuAnimator.Enable();
                settingsMenu.Closed = () => selfAnimator.Enable();
            });
            
            tutorialButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickButton);
                selfAnimator.Disable();
                tutorialMenuAnimator.Enable();
                tutorialMenu.Closed = () => selfAnimator.Enable();
            });

            Utils.LevelExit += () =>
            {
                gameUi.Disable();
                selfAnimator.Enable();
            };
        }

        private void UpdateFat()
        {
            _characterFat = Mathf.Clamp(_characterFat, 0, Utils.MaxFat);
            fatSlider.fillAmount = _characterFat / Utils.MaxFat;
            
            foreach (var stage in characterData.stages)
            {
                if (stage.maxFatLevel < _characterFat) continue;

                characterImage.sprite = stage.sprite;
                return;
            }

            characterImage.sprite = characterData.stages[^1].sprite;
        }
    }
}