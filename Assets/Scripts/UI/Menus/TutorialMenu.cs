﻿using System;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class TutorialMenu : MonoBehaviour
    {
        public Action Closed;
        
        public UiAnimator animator;
        public Button[] closeButtons;

        public Button nextPageButton;
        public Button prevPageButton;
        public GameObject[] pages;

        private int _currentPageId;

        private void Awake()
        {
            foreach (var button in closeButtons)
            {
                button.onClick.AddListener(() =>
                {
                    SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickButton);
                    animator.Disable();
                    Closed?.Invoke();
                });
            }
            
            nextPageButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickButton);
                pages[_currentPageId].SetActive(false);
                _currentPageId = (_currentPageId + 1) % pages.Length;
                pages[_currentPageId].SetActive(true);
                
                ToggleButtons();
            });
            
            prevPageButton.onClick.AddListener(() =>
            {
                SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickButton);
                pages[_currentPageId].SetActive(false);
                _currentPageId = (_currentPageId - 1) % pages.Length;
                pages[_currentPageId].SetActive(true);
                
                ToggleButtons();
            });

            _currentPageId = 0;
            pages[0].SetActive(true);
            ToggleButtons();
        }

        private void ToggleButtons()
        {
            nextPageButton.gameObject.SetActive(_currentPageId < pages.Length - 1);
            prevPageButton.gameObject.SetActive(_currentPageId > 0);
        }
    }
}