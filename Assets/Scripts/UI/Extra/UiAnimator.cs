﻿using System;
using System.Collections;
using UnityEngine;

namespace Scripts
{
    public class UiAnimator : MonoBehaviour
    {
        public bool inverse;
        public bool animateOnEnable;
        public float animateTime;
        public EasingType easingType;
        public Vector3 offset;

        private Coroutine _currentRoutine;

        private Vector3 _startPosition;
        private Vector3 _endPosition;

        private bool _positionsRecorded;

        private void Awake()
        {
            TryRecordPositions();
        }

        private void OnEnable()
        {
            TryRecordPositions();
            
            if (animateOnEnable)
                Enable();
        }

        public void MoveToOffset()
        {
            TryRecordPositions();
            transform.position = _endPosition;
        }

        public void Enable(Action callback = null)
        {
            TryRecordPositions();
            
            gameObject.SetActive(true);
            
            if (_currentRoutine != null)
                StopCoroutine(_currentRoutine);

            transform.position = _startPosition;
            _currentRoutine = StartCoroutine(Animate(false, false, callback));
        }

        public void Disable(Action callback = null)
        {
            if (_currentRoutine != null)
                StopCoroutine(_currentRoutine);

            transform.position = _startPosition;
            _currentRoutine = StartCoroutine(Animate(true, true, callback));
        }

        private void TryRecordPositions()
        {
            if (_positionsRecorded) return;
            
            _startPosition = transform.position;
            _endPosition = transform.position + offset;
            _positionsRecorded = true;
        }

        private IEnumerator Animate(bool inversePosition, bool disableAtEnd, Action callback = null)
        {
            var startPosition = inversePosition ? _startPosition : _endPosition;
            var endPosition = inversePosition ? _endPosition : _startPosition;
            
            var func = Easings.GetEasing(easingType);
            float time = 0f;
            while (time < animateTime)
            {
                time += Time.deltaTime;
                float t = time / animateTime;
                if (inverse) t = 1 - t;

                float value = func(t);
                transform.position = Vector3.Lerp(startPosition, endPosition, value);
                
                yield return null;
            }
            
            transform.position = inverse ? startPosition : endPosition;
            
            if (disableAtEnd)
                gameObject.SetActive(false);
            
            _currentRoutine = null;
            callback?.Invoke();
        }
    }
}