﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    public enum EasingType
    {
        Elastic,
        QUint,
        Cubic,
    }
    
    public static class Easings
    {
        private const float C4 = 2f * Mathf.PI / 3f;

        private static Dictionary<EasingType, Func<float, float>> _easings = new()
        {
            {
                EasingType.Elastic,
                t => Mathf.Approximately(t, 0f)
                    ? 0f
                    : Mathf.Approximately(t, 1f)
                        ? 1f
                        : Mathf.Pow(2, -10 * t) * Mathf.Sin((t * 10 - 0.75f) * C4) + 1f
            },
            {
                EasingType.QUint,
                t => t < 0.5f
                    ? 16 * t * t * t * t * t
                    : 1 - Mathf.Pow(-2 * t + 2, 5) * 0.5f
            },
            {
                EasingType.Cubic,
                t => t < 0.5f
                    ? 4 * t * t * t
                    : 1 - Mathf.Pow(-2 * t + 2, 3) * 0.5f
            }
        };

        public static Func<float, float> GetEasing(EasingType type)
        {
            return _easings[type];
        }
    }
}