﻿using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class SizeableSlider : MonoBehaviour
    {
        public float defaultSize;
        public float defaultPosition;
        public RectTransform transform;
        public Slider slider;

        public void SetSize(float sizeMultiplier)
        {
            float newSize = defaultSize * sizeMultiplier;
            transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, newSize);
            float sizeDelta = defaultSize - newSize;
            var position = transform.anchoredPosition;
            position.x = defaultPosition - sizeDelta / 2f;
            transform.anchoredPosition = position;
            slider.maxValue = sizeMultiplier;
        }

        public void SetValue(float value)
        {
            slider.value = value;
        }
    }
}