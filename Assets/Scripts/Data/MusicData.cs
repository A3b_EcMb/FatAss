﻿using System;
using DefaultNamespace;
using Scripts;
using UnityEngine;

namespace Data
{
    [Serializable]
    public class SoundEffectContainer
    {
        public SoundEffectType type;
        public AudioClip[] clips;
    }
    
    [CreateAssetMenu(fileName = "Music Data", menuName = "Data/Music")]
    public class MusicData : ScriptableObject
    {
        public AudioClip[] backroundClips;
        public SoundEffectContainer[] effects;

        public AudioClip GetEffectClip(SoundEffectType effectType)
        {
            foreach (var container in effects)
            {
                if (container.type != effectType) continue;

                return container.clips.GetRandom();
            }

            return null;
        }
    }
}