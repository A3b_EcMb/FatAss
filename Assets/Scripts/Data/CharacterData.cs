using System;
using UnityEngine;

namespace Data
{
    [Serializable]
    public class StageContainer
    {
        public Sprite sprite;
        public float maxFatLevel;
    }
    
    [CreateAssetMenu(fileName = "Character Data", menuName = "Data/Character")]
    public class CharacterData : ScriptableObject
    {
        public StageContainer[] stages;
    }
}