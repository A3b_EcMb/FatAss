using System;
using UnityEngine;

namespace Data
{
    [Serializable]
    public class BackroundContainer
    {
        public Sprite sprite;
        public Vector2 size;
    }
    
    [CreateAssetMenu(fileName = "Backgrounds", menuName = "Data/Backgrounds")]
    public class BackgroundData : ScriptableObject
    {
        public Sprite[] backgrounds;
    }
}