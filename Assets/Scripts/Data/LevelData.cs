using System;
using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [Serializable]
    public class ItemContainer
    {
        public ItemTypeData itemType;
        public float chance;
    }
    
    [CreateAssetMenu(fileName = "Level Data", menuName = "Data/Level")]
    public class LevelData : ScriptableObject
    {
        public float minItemSpawnTimer;
        public float maxItemSpawnTimer;
        public float itemSpeed;
        public float maxDifficultyMultiplier;
        public float timeToMaxDifficultyMultiplier;
        public float startLevelDelay;
        public AnimationCurve difficultyCurve;
        public List<ItemContainer> items;
    }
}