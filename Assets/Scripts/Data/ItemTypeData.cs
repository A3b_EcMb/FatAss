using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "Items Type Data", menuName = "Data/Items Type")]
    public class ItemTypeData : ScriptableObject
    {
        public Sprite[] sprites;
        public Color indicationColor;
        public float fat;
        public float energy;
    }
}