﻿using UnityEngine;

namespace Scripts
{
    public class ScalingEffect : MonoBehaviour
    {
        public float maxOffset;
        public float maxSpeed;
        public float minSpeed;
        public float maxScale;
        public float minScale;

        private bool _expanding;
        private float _offsetTimer;
        private float _speed;

        private Transform _transform;

        private void Start()
        {
            _offsetTimer = Random.Range(0f, maxOffset);
            _speed = Random.Range(minSpeed, maxSpeed);
            _transform = transform;
        }

        private void Update()
        {
            if (_offsetTimer > 0f)
            {
                _offsetTimer -= Time.deltaTime;
                return;
            }
            
            float scaleChangeValue = _speed * Time.deltaTime;
            if (_expanding)
            {
                _transform.localScale += Vector3.one * scaleChangeValue;
                if (_transform.localScale.x < maxScale) return;

                _expanding = false;
            }
            else
            {
                _transform.localScale -= Vector3.one * scaleChangeValue;
                if (_transform.localScale.x > minScale) return;

                _expanding = true;
            }
        }
    }
}