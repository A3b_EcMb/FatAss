﻿using UnityEngine;

namespace Scripts
{
    public class ShakingEffect : MonoBehaviour
    {
        public float maxShakeInterval;
        public float minShakeInterval;
        
        public float shakeDuration = 0.5f;
        public float shakeAmount = 0.1f;

        private bool _isShaking;
        
        private float _shakeTimer;
        private float _timeToNextShake;
        
        private Vector3 _startPosition;
        private Transform _transform;

        private void Start()
        {
            _startPosition = transform.localPosition;
            _timeToNextShake = Random.Range(minShakeInterval, maxShakeInterval);
            _transform = transform;
        }

        private void Update()
        {
            if (_timeToNextShake > 0f)
            {
                _timeToNextShake -= Time.deltaTime;
                return;
            }

            if (!_isShaking)
            {
                StartShake();
                return;
            }

            if (_shakeTimer <= 0f)
            {
                _isShaking = false;
                _transform.localPosition = _startPosition;
                _timeToNextShake = Random.Range(minShakeInterval, maxShakeInterval);
                return;
            }

            _shakeTimer -= Time.deltaTime;

            float shakeForce = (1 - _shakeTimer / shakeDuration) * shakeAmount * 2f;

            float offsetX = Random.Range(-shakeForce, shakeForce);
            float offsetY = Random.Range(-shakeForce, shakeForce);

            _transform.localPosition = _startPosition + new Vector3(offsetX, offsetY, 0);
        }

        // Метод для запуска тряски
        private void StartShake()
        {
            _shakeTimer = shakeDuration;
            _startPosition = _transform.localPosition;
            _isShaking = true;
        }
    }
}