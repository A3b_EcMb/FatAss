﻿using UnityEngine;

namespace Localization
{
    [CreateAssetMenu(menuName = "Localization/Language Data", fileName = "Language Data")]
    public class LanguageData : ScriptableObject
    {
        [SerializeField] public Language language;
        [SerializeField] public LocalizationContainer[] localizations;
    }
}