﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Localization
{
    [Serializable]
    public class LocalizationContainer
    {
        [SerializeField] public string key;
        [SerializeField] public string value;
    }
    
    [CreateAssetMenu(menuName = "Localization/Localization Data", fileName = "Localization")]
    public class LocalizationData : ScriptableObject
    {
        [SerializeField] public LanguageData[] languages;

        private Dictionary<string, string> _localizations;

        public void SwitchLanguage(Language language)
        {
            var localization = GetLanguageData(language);
            _localizations = new Dictionary<string, string>();
            foreach (var container in localization.localizations)
            {
                _localizations.Add(container.key, container.value);
            }
        }

        public string GetLocalization(string key)
        {
            if (_localizations == null)
                throw new ArgumentException("Localization don't initialized");
            
            return _localizations.TryGetValue(key, out var value) ? value : key;
        }

        private LanguageData GetLanguageData(Language language)
        {
            foreach (var languageContainer in languages)
            {
                if (languageContainer.language == language)
                    return languageContainer;
            }

            return languages[0];
        }
    }
}