﻿using UnityEngine;

namespace Localization
{
    public class LocalizationSource : MonoBehaviour
    {
        public static LocalizationSource Instance { get; private set; }

        public LocalizationData data;
        public Language defaultLanguage;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            data.SwitchLanguage(defaultLanguage);
        }

        public void SwitchLanguage(Language language) => data.SwitchLanguage(language);

        public string GetLocalization(string key) => data.GetLocalization(key);
    }
}