using System;
using Data;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class Player : MonoBehaviour
    {
        [Header("Values")]
        public float startEnergy;
        public float startFat;

        [Header("Energy decreast")]
        public float energyDecreaseSpeed;
        public float maxFatEnergyDecreaseSpeedMultiplier;
        public float maxFatEnergyMultiplier;

        [Header("References")]
        public Image image;
        public CharacterData data;

        [Header("UI")]
        public Image energySlider;
        public Image fatSlider;

        private float _currentMaxEnergy;
        private float _fat;
        private float _energy;

        private bool _paused;
        private bool _started;

        private void Start()
        {
            ResetValues();
            
            Utils.ItemClicked += OnItemClicked;
            Utils.LevelRestarted += ResetValues;
            Utils.LevelExit += () => _started = false;
            Utils.LevelPaused += value => _paused = value;
        }

        private void Update()
        {
            if (_paused || !_started) return;
            
            float speed = energyDecreaseSpeed * Mathf.Lerp(1f, maxFatEnergyDecreaseSpeedMultiplier, _fat / Utils.MaxFat);
            _energy -= speed * Time.deltaTime;

            energySlider.fillAmount = _energy / Utils.MaxEnergy;

            if (_energy > 0) return;
            
            SoundEffectPlayer.Instance.PlaySound(SoundEffectType.Lose);
            Utils.PlayerDied?.Invoke();
            _started = false;
        }

        private void ResetValues()
        {
            _fat = startFat;
            _energy = startEnergy;
            _currentMaxEnergy = Utils.MaxEnergy;
            UpdateFat();
            UpdateSprite();

            _started = true;
            _paused = false;
        }

        private void OnItemClicked(ItemTypeData typeData)
        {
            _energy += typeData.energy;
            if (_energy > _currentMaxEnergy)
                _energy = _currentMaxEnergy;
            
            _fat += typeData.fat;
            if (_fat > Utils.MaxFat)
                _fat = Utils.MaxFat;

            UpdateFat();
            UpdateSprite();
        }

        private void UpdateFat()
        {
            fatSlider.fillAmount = _fat / Utils.MaxFat;
            float maxEnergyDecrease = Mathf.Lerp(1f, maxFatEnergyMultiplier, _fat / Utils.MaxFat);
            _currentMaxEnergy = Utils.MaxEnergy * maxEnergyDecrease;
        }

        private void UpdateSprite()
        {
            foreach (var stage in data.stages)
            {
                if (stage.maxFatLevel < _fat) continue;

                image.sprite = stage.sprite;
                return;
            }

            image.sprite = data.stages[^1].sprite;
        }
    }
}