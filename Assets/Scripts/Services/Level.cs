using System.Collections.Generic;
using Data;
using Scripts;
using UnityEngine;
using Random = UnityEngine.Random;

public class Level : MonoBehaviour
{
    public float padding;
    public Camera camera;
    public LevelData data;
    public Item itemPrefab;

    private bool _paused;
    private bool _running;
    private float _startDelayTimer;
    private float _itemSpawnTimer;
    private float _timeSinceStart;
    private float _maxChance;
    
    private List<Item> _spawnedItems = new();
    private List<Item> _itemsToRemove = new();

    private void Start()
    {
        _maxChance = 0f;
        foreach (var item in data.items)
        {
            _maxChance += item.chance;
        }

        Utils.LevelPaused += state => _paused = state;
        Utils.PlayerDied += () => EndLevel();
        Utils.LevelRestarted += () => RestartLevel();
        Utils.LevelExit += () => EndLevel();
    }

    private void Update()
    {
        if (_paused || !_running) return;
        
        if (_startDelayTimer > 0f)
        {
            _startDelayTimer -= Time.deltaTime;
            return;
        }
        
        if (Input.GetMouseButtonDown(0))
        {
            var mousePosition = camera.ScreenToWorldPoint(Input.mousePosition);
            foreach (var spawnedItem in _spawnedItems)
            {
                spawnedItem.MouseClicked(mousePosition);
            }
        }

        foreach (var itemToRemove in _itemsToRemove)
        {
            _spawnedItems.Remove(itemToRemove);
            Destroy(itemToRemove.gameObject);
        }
        _itemsToRemove.Clear();
        
        _timeSinceStart += Time.deltaTime;
        float timeValue = Mathf.Clamp01(_timeSinceStart / data.timeToMaxDifficultyMultiplier);
        float difficultyMultiplier = data.difficultyCurve.Evaluate(timeValue) * data.maxDifficultyMultiplier + 1f;

        float speed = data.itemSpeed * difficultyMultiplier;
        float moveDiff = speed * Time.deltaTime;
        foreach (var spawnedItem in _spawnedItems)
        {
            spawnedItem.transform.position += Vector3.down * moveDiff;
            if (spawnedItem.transform.position.y < -camera.orthographicSize)
                _itemsToRemove.Add(spawnedItem);
        }

        if (_itemSpawnTimer > 0f)
        {
            _itemSpawnTimer -= Time.deltaTime;
            return;
        }

        var itemData = GetItemToSpawn();
        float halfWidth = camera.orthographicSize / 2f - padding;
        float widthOffset = Random.Range(-halfWidth, halfWidth);
        
        var item = Instantiate(itemPrefab);
        item.transform.position = Vector3.up * camera.orthographicSize + Vector3.right * widthOffset;
        item.Initialize(itemData.itemType, this);

        _spawnedItems.Add(item);
        _itemSpawnTimer = Random.Range(data.minItemSpawnTimer, data.maxItemSpawnTimer / difficultyMultiplier);
    }

    private ItemContainer GetItemToSpawn()
    {
        float chanceResult = Random.Range(0f, _maxChance);
        float totalChance = 0f;
        foreach (var item in data.items)
        {
            totalChance += item.chance;
            if (totalChance > chanceResult)
                return item;
        }

        return data.items[^1];
    }

    public void RemoveItem(Item item)
    {
        _itemsToRemove.Add(item);
    }

    private void RestartLevel()
    {
        EndLevel();
        _running = true;
        _itemSpawnTimer = 0f;
        _timeSinceStart = 0f;
        _paused = false;

        _startDelayTimer = data.startLevelDelay;
    }

    private void EndLevel()
    {
        _running = false;
        foreach (var item in _spawnedItems)
        {
            Destroy(item.gameObject);
        }
        _spawnedItems.Clear();
        _itemsToRemove.Clear();
    }
}