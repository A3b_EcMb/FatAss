using Data;
using UnityEngine;

namespace Scripts
{
    public class Background : MonoBehaviour
    {
        public Camera camera;
        public SpriteRenderer target;
        public BackgroundData data;

        private void Start()
        {
            SetRandomBackground();
        }

        [ContextMenu("Set random background")]
        private void SetRandomBackground()
        {
            var backround = data.backgrounds.GetRandom();
            target.sprite = backround;

            float width = target.sprite.rect.width;
            float height = target.sprite.rect.height;

            float targetScale;
            if (width > height)
            {
                targetScale = camera.orthographicSize / height * backround.pixelsPerUnit * 2f;
            }
            else
            {
                float screenRatio = Screen.height * 1.0f / Screen.width;
                targetScale = camera.orthographicSize / width / screenRatio * backround.pixelsPerUnit * 2f;
            }
            
            target.transform.localScale = Vector3.one * targetScale;
        }
    }
}