﻿using System;
using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.Advertisements;

namespace Scripts
{
    public class AdsPlayer : MonoBehaviour
    {
        private const string InterstitialId = "ca-app-pub-6297120966719146/4236416766";
        private const string TestInterstitialId = "ca-app-pub-3940256099942544/1033173712";
        
        public static AdsPlayer Instance { get; private set; }

        public bool isTestAds;
        
        private bool _initialized;
        private bool _showInterstitial;
        private InterstitialAd _interstitialAd;
        
        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

#if UNITY_EDITOR
            isTestAds = true;
#endif

            Instance = this;

            MobileAds.RaiseAdEventsOnUnityMainThread = true;
            MobileAds.Initialize(_ => _initialized = true);
        }

        private void Update()
        {
            if (!_showInterstitial) return;

            _showInterstitial = false;
            if (_interstitialAd == null || !_interstitialAd.CanShowAd())
            {
                Debug.Log("Requested to show ad, but cannot show it");
                return;
            }
            
            _interstitialAd.Show();
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        public void ShowInterstitial()
        {
            if (!_initialized)
            {
                Debug.Log("Cannot show interstitial ad: not initialized");
                return;
            }

            if (_interstitialAd != null && _interstitialAd.CanShowAd())
            {
                _interstitialAd.Show();
                return;
            }
            
            LoadInterstitial(true);
        }

        private void LoadInterstitial(bool showOnLoaded)
        {
            Debug.Log("Loading interstitial ad");

            if (_interstitialAd != null)
            {
                _interstitialAd.Destroy();
                _interstitialAd = null;
            }

            var adRequest = new AdRequest();
            adRequest.Keywords.Add("unity-admob-sample");
            
            InterstitialAd.Load(isTestAds ? TestInterstitialId : InterstitialId, adRequest,
                (ad, error) =>
                {
                    if (error != null || ad == null)
                    {
                        Debug.Log($"Interstitial ad failed to load: {error}");
                        return;
                    }
                    
                    Debug.Log($"Interstitial ad loaded with response: {ad.GetResponseInfo()}");
                    _interstitialAd = ad;
                    RegisterAdEventHandlers();
                    
                    if (showOnLoaded)
                        _showInterstitial = true;
                });
        }
        
        private void RegisterAdEventHandlers()
        {
            // Raised when the ad is estimated to have earned money.
            _interstitialAd.OnAdPaid += (AdValue adValue) =>
            {
                Debug.Log(String.Format("Interstitial ad paid {0} {1}.",
                    adValue.Value,
                    adValue.CurrencyCode));
            };
            // Raised when an impression is recorded for an ad.
            _interstitialAd.OnAdImpressionRecorded += () =>
            {
                Debug.Log("Interstitial ad recorded an impression.");
            };
            // Raised when a click is recorded for an ad.
            _interstitialAd.OnAdClicked += () =>
            {
                Debug.Log("Interstitial ad was clicked.");
            };
            // Raised when an ad opened full screen content.
            _interstitialAd.OnAdFullScreenContentOpened += () =>
            {
                Debug.Log("Interstitial ad full screen content opened.");
            };
            // Raised when the ad closed full screen content.
            _interstitialAd.OnAdFullScreenContentClosed += () =>
            {
                Debug.Log("Interstitial ad full screen content closed.");
                LoadInterstitial(false);
            };
            // Raised when the ad failed to open full screen content.
            _interstitialAd.OnAdFullScreenContentFailed += (AdError error) =>
            {
                Debug.Log("Interstitial ad failed to open full screen content " +
                               "with error : " + error);
                LoadInterstitial(false);
            };
        }
    }
}