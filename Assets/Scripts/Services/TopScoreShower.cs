﻿using TMPro;
using UnityEngine;

namespace Scripts
{
    public class TopScoreShower : MonoBehaviour
    {
        public TextMeshProUGUI text;

        private void OnEnable()
        {
            Utils.UpdateTopScore += Show;
            Show();
        }

        private void OnDisable()
        {
            Utils.UpdateTopScore -= Show;
        }

        private void Show()
        {
            text.text = Utils.GetTopScore().ToString();
        }
    }
}