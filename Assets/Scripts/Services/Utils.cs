using System;
using Data;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Scripts
{
    public static class Utils
    {
        private const string ViewedTutorialKey = "PlayerViewedTutorial";
        private const string TopScoreKey = "TopScore";
        
        public static Action<ItemTypeData> ItemClicked;
        public static Action PlayerDied;
        public static Action LevelRestarted;
        public static Action<bool> LevelPaused;
        public static Action LevelExit;
        public static Action UpdateTopScore;

        public const float MaxFat = 100f;
        public const float MaxEnergy = 100f;
        public const int FailesToShowAd = 3;
        
        public static T GetRandom<T>(this T[] array)
        {
            return array[Random.Range(0, array.Length)];
        }

        public static bool PlayerViewedTutorial()
        {
            return PlayerPrefs.HasKey(ViewedTutorialKey);
        }

        public static void SetPlayerViewedTutorial()
        {
            PlayerPrefs.SetInt(ViewedTutorialKey, 0);
        }

        public static int GetTopScore()
        {
            return PlayerPrefs.HasKey(TopScoreKey) ? PlayerPrefs.GetInt(TopScoreKey) : 0;
        }

        public static void SetTopScore(int value)
        {
            PlayerPrefs.SetInt(TopScoreKey, value);
            UpdateTopScore?.Invoke();
        }
    }
}