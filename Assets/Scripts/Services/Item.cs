using Data;
using DefaultNamespace;
using Scripts;
using UnityEngine;

public class Item : MonoBehaviour
{
    public Vector2 targetSize;
    public SpriteRenderer image;
    public SpriteRenderer indicator;
    public Transform target;
    
    private ItemTypeData _typeData;
    private Level _currentLevel;

    public void Initialize(ItemTypeData typeData, Level currentLevel)
    {
        _typeData = typeData;
        _currentLevel = currentLevel;
        image.sprite = typeData.sprites.GetRandom();
        indicator.color = typeData.indicationColor;

        var spriteSize = image.bounds.size;
        float scaleX = targetSize.x / spriteSize.x;
        float scaleY = targetSize.y / spriteSize.y;
        float scale = Mathf.Min(scaleX, scaleY);
        target.localScale = new Vector3(scale, scale, 1f);
    }

    public void MouseClicked(Vector3 position)
    {
        var currentPosition = transform.position;
        var size = targetSize;
        if (position.x > currentPosition.x + size.x) return;
        if (position.x < currentPosition.x - size.x) return;
        if (position.y > currentPosition.y + size.y) return;
        if (position.y < currentPosition.y - size.y) return;

        SoundEffectPlayer.Instance.PlaySound(SoundEffectType.ClickItem);
        Utils.ItemClicked?.Invoke(_typeData);
        _currentLevel.RemoveItem(this);
    }
}