﻿using Data;
using UnityEngine;

namespace DefaultNamespace
{
    public class SoundEffectPlayer : MonoBehaviour
    {
        public static SoundEffectPlayer Instance { get; private set; }
        
        public AudioSource source;
        public MusicData effects;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }
            Instance = this;
        }

        public void PlaySound(SoundEffectType type)
        {
            var clip = effects.GetEffectClip(type);
            if (clip == null)
            {
                Debug.LogError("Cannot find clip for ");
                return;
            }
            
            source.PlayOneShot(clip);
        }
    }
}