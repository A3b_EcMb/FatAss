﻿namespace DefaultNamespace
{
    public enum SoundEffectType
    {
        ClickButton,
        ClickItem,
        Lose,
        Reward,
        StartPlay,
        BackToMenu,
    }
}