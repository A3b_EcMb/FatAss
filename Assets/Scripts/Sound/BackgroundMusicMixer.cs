﻿using System.Collections;
using Data;
using Scripts;
using UnityEngine;

namespace DefaultNamespace
{
    public class BackgroundMusicMixer : MonoBehaviour
    {
        public AudioSource firstSource;
        public AudioSource secondSource;
        public float clipFadeTime;
        public float clipChangeTimeFromEnd;

        public MusicData data;

        private AudioSource _current;
        private AudioSource _next;
        private Coroutine _currentChangeRoutine;

        private void Awake()
        {
            _current = firstSource;
            _next = secondSource;
            _current.clip = data.backroundClips.GetRandom();
            
            PlayRandomClip();
        }

        private void Update()
        {
            if (_current.clip.length - _current.time > clipChangeTimeFromEnd) return;
            
            PlayRandomClip();
        }

        private void PlayRandomClip()
        {
            if (_currentChangeRoutine != null)
                StopCoroutine(_currentChangeRoutine);

            _next.clip = data.backroundClips.GetRandom();
            _next.Play();
            
            _currentChangeRoutine = StartCoroutine(FadeSources());
        }

        private IEnumerator FadeSources()
        {
            float time = 0f;
            while (time < clipFadeTime)
            {
                yield return null;
                
                time += Time.deltaTime;
                float t = time / clipFadeTime;
                
                _current.volume = 1 - t;
                _next.volume = t;
            }

            _current.volume = 0f;
            _next.volume = 1f;
            
            _current.Stop();
            (_current, _next) = (_next, _current);
            _currentChangeRoutine = null;
        }
    }
}